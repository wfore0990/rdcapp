import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";


const Header = (props) => {
  const {
    logoImage,
    pageTitle,
    username,
  } = props

  // Update page title
  useEffect(() => {
    document.title = pageTitle
  }, [pageTitle])

  return (
    <header>
      <Link to="/">
        <img
          alt="company logo"
          className="header-logo"
          src={logoImage}
        />
      </Link>

      {pageTitle ? <h2 className="header-title">{pageTitle}</h2> : null}

      <Link to="/profile">hello, {username ? {username} : 'user'}</Link>
    </header>
  );
};

Header.propTypes = {
  logoImage: PropTypes.string,
  pageTitle: PropTypes.string,
  username: PropTypes.string
};

export default Header;