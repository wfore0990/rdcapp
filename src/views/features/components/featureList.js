import React from 'react';
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";

import FeatureDetail from './featureDetail'

const FeatureList = (props) => {
  const {
    featureData,
    selectedFeatureID
  } = props

  if (selectedFeatureID) {
    const selectedFeature = featureData.find((item) => {
      return item.id === selectedFeatureID
    })
    return <FeatureDetail selectedFeature={selectedFeature} />
  }
  
  return (
    <div className="page-feature">
    <h2> USGS All Earthquakes, Past Hour</h2>
      <table className="feature-table">
        <thead>
          <tr>
            <th>Place</th>
            <th>Magnitude</th>
            <th>Time</th>
          </tr>
        </thead>
        <tbody>
          {featureData.map((featureItem, idx) => {
            const {place, mag, time} = featureItem.properties
            let humanTime = new Date(time).toISOString()
            return (
              <tr key={idx}>
                <td><Link to={`/detail/${featureItem.id}`}>{place}</Link></td>
                <td>{mag}</td>
                <td>{humanTime}</td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </div>
  );
};

FeatureList.propTypes = {
  featureData: PropTypes.array,
  selectedFeatureID: PropTypes.string,
};

export default FeatureList;