import React from 'react';
import PropTypes from 'prop-types';


const FeatureDetail = (props) => {
  const {
    selectedFeature
  } = props

  // If feature object not provided in props, present error.
  if (!selectedFeature) {
    return (
      <h3>
        No item matches provided feature ID!
        <br/> 
        Please check URL and try again.
      </h3>
    )
  }

  // Reference values from props object
  const { mag, place, time, status, tsunami, type } = selectedFeature.properties

  return (
    <div className="page-detail">
      <h2>{mag} - {place}</h2>
      <table className="detail-table">
        <tbody>
          <tr>
            <td>Place</td>
            <td>{place}</td>
          </tr>
          <tr>
            <td>Magnitude</td>
            <td>{mag}</td>
          </tr>
          <tr>
            <td>time</td>
            <td>{time}</td>
          </tr>
          <tr>
            <td>status</td>
            <td>{status}</td>
          </tr>
          <tr>
            <td>tsunami</td>
            <td>{tsunami}</td>
          </tr>
          <tr>
            <td>type</td>
            <td>{type}</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};

FeatureDetail.propTypes = {
  selectedFeature: PropTypes.object,
};

export default FeatureDetail;