import React from 'react';
import PropTypes from 'prop-types';
import { useParams } from "react-router-dom";

import FeatureList from './components/featureList'

const Features = (props) => {
  const {
    featureData,
  } = props
  // If a specific featureID provided in URL path, retrieve it
  const { selectedFeatureID } = useParams();

  return (
   <FeatureList 
    featureData={featureData}
    selectedFeatureID={selectedFeatureID}
   />
  );
};

Features.propTypes = {
  featureData: PropTypes.array,
};

Features.defaultProps = {
  featureData: null,
  selectedFeature: null,
};

export default Features;