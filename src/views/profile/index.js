import React from 'react';
import PropTypes from 'prop-types';

const Profile = (props) => {
  const {
    userProfile
  } = props

  return (
    <div className="page-profile">
    <h2> Profile </h2>
      <div className="profile-content">
        <div className="profile-image">
          <img 
            alt="user profile avatar"
            src={userProfile.avatarImage} 
            className="profile-avatar"
          />
        </div>
        <div className="profile-details">
          <table className="profile-table">
            <tbody>
              <tr>
                <td>first name</td>
                <td>{userProfile.firstName}</td>
              </tr>
              <tr>
                <td>last name</td>
                <td>{userProfile.lastName}</td>
              </tr>
              <tr>
                <td>phone</td>
                <td>{userProfile.phone}</td>
              </tr>
              <tr>
                <td>email</td>
                <td>{userProfile.email}</td>
              </tr>
              <tr>
                <td>bio</td>
                <td>{userProfile.bio}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

Profile.propTypes = {
  userProfile: PropTypes.object,
};

export default Profile;