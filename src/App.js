import React from 'react';
import { BrowserRouter, Switch, Redirect, Route } from "react-router-dom";
import './App.css';
// static data
import AppData from './appData.json';
// layout
import Header from './layout/header';
// views
import Features from './views/features/';
import Profile from './views/profile/';


export default function App() {

  return (
    <BrowserRouter>
      <div className="app">
        <Header
          logoImage={AppData.site.logoImage}
          pageTitle={AppData.site.title}
          username={AppData.profile.username}
        />

        <div className="app-body">
          <Switch>
          
            {/* Feature list and detail views */}
            <Route exact path={["/", "/home", "/detail/:selectedFeatureID?"]}>
              <Features
                featureData={AppData.data.features}
              />
            </Route>

            {/* User profile detail view */}
            <Route path="/profile">
              <Profile 
                userProfile={AppData.profile}
              />
            </Route>
            <Redirect to="/" />
          </Switch>
        </div>

      </div>
    </BrowserRouter>
  );
}
